@extends('newweb.mainweb')
@section('content')


@if(Session::has('delstd'))
<script type="text/javascript">
swal({
  title: "Good job!",
  text: "ลบข้อมูลนักศึกษาเรียบร้อยแล้ว",
  icon: "success",
  button: "Ok!",
});
</script>
@endif
<div class="box box-primary">
<div class="box-header">
รายชื่อนักศึกษา
</div>
<div class="box-body">
    <table class="table table-bordered">
    <tr>
    <td>รหัสนักศึกษา</td><td>ชื่อ - นามสกุล</td><td>#</td><td>#</td>
    </tr>
    @foreach($data_std as $item_data)
    <tr>
    <td> {{$item_data ->STUDENTCODE }} </td>
    <td> {{$item_data ->STUDENTNAME }}  {{$item_data ->STUDENTSURNAME }}</td>
    <td><a href="detail/{{$item_data ->STUDENTCODE }}" class="btn btn-primary">รายละเอียด</a></td>
    <td><a href="del/{{$item_data ->STUDENTCODE }}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');">ลบ</a></td>
    </tr>
    @endforeach

    </table>
{{$data_std->render()}}
</div>
</div>

@endsection