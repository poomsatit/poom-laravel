@extends('newweb.mainweb')
@section('content')

<div class="box box-primary">
<div class="box-header">
รายละเอียดข้อมูลนักศึกษา
</div>
<div class="box-body">
<table class="table">
<tr>
<td>รหัสนักศึกษา</td>
<td>{{$item ->STUDENTCODE }}</td>
</tr>
<tr>
<td>ชื่อ - นามสกุล</td>
<td>{{$item ->PREFIXNAME }}{{$item ->STUDENTNAME }} {{$item ->STUDENTSURNAME }}</td>
</tr>
<tr>
<td>สาขา</td>
<td>{{$item ->PROGRAMNAME }}</td>
</tr>
<tr>
<td>คณะ</td>
<td>{{$item ->FACULTYNAME }}</td>
</tr>
<tr>
<td>ประเภท</td>
<td>{{$item ->LEVELNAME }}</td>
</tr >
<td><a href="/list" class="btn btn-primary">ย้อนกลับ</a></td> 
<td><a href="/del/{{$item ->STUDENTCODE }}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');">ลบ</a></td>
</tr>
</table>
</div>
</div>

@endsection