<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class datastd extends Model
{
    //
    protected $table ='tb_std';
    protected $primaryKey = 'STUDENTCODE';
    protected $fillable = ['STUDENTCODE','STUDENTNAME','STUDENTSURNAME'];
}
