<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\datastd;
use DB;
use Session;

class datastdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
            $data_std = datastd::orderBy('STUDENTCODE','DESC')->paginate(25) ;

        return view('mainadmin/list_std',['data_std'=>$data_std]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

$data_std = DB::table('tb_std')
            ->join('tb_prefix', 'tb_std.PREFIXID', '=', 'tb_prefix.PREFIXID')
            ->join('tb_program', 'tb_std.PROGRAMID', '=', 'tb_program.PROGRAMID')
            ->join('tb_level', 'tb_std.LEVELID', '=', 'tb_level.LEVELID')
            ->join('tb_faculty', 'tb_program.FACULTYID', '=', 'tb_faculty.FACULTYID')
            ->select('tb_std.STUDENTCODE','tb_std.STUDENTNAME', 'tb_std.STUDENTSURNAME', 'tb_prefix.PREFIXNAME','tb_faculty.FACULTYNAME', 
            'tb_program.PROGRAMNAME', 
            'tb_level.LEVELNAME')
            ->where('tb_std.STUDENTCODE', '=',$id)
            ->first();

        return view('mainadmin/detail',['item'=>$data_std]);
    }

    /**
     * 
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = datastd::findOrFail($id);
        $del->delete();
        Session::flash('delstd','ได้ทำการลบ');
       return redirect('/list');
    }

    
}
