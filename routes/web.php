<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
Route::get('/text', function () {
    return "ยินดีตอนรับ";
});
Route::get('/r', function () {
    return view('room');
});

Route::get('/admin', function () {
    return view('newweb/mainweb');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'HomeController@index')->name('home');

Route::get('/list', 'datastdController@index')->name('home');
Route::get('/detail/{id}', 'datastdController@show');
Route::get('/del/{id}', 'datastdController@destroy');